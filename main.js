/*****************************************************************************
 *              Mes variables
 *****************************************************************************/
const playMusic = document.querySelector('a');
const pauseMusic = document.querySelector('.pause');
const monAudio = document.querySelector('audio');

/****************************************************************************
 *              Mes fonctions
 ****************************************************************************/
playMusic.addEventListener('click', () => {
        monAudio.play();
    });

pauseMusic.addEventListener('click', () => {
        //console.log('ok');
        monAudio.pause();
    });